package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test01_Login extends BaseTest {
    @Test
    public void testLogin () {
        //Step 1 Confirm we are on the Welcome Page
        assertTrue (welcomePage.checkCorrectPage());
        //step 2 Click on Login
        welcomePage.clickOnLogin();
        assertTrue(loginPage.checkCorrectPage());
        //Step 3 Login with credentials
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));
    }


}
